package com.thalmiclabs.myo.namespaces
{
	/**
	 * Namespace for internal operations of the library
	 */
	public namespace thalmiclabs;
}